import bcrypt from "bcryptjs";

const users = [
    {
        name: "Admin User",
        email: "admin@ecommerce.com",
        password: bcrypt.hashSync("admin", 10),
        isAdmin: true,
    },
    {
        name: "John Doe",
        email: "johndoe@yahoo.com",
        password: bcrypt.hashSync("1234", 10),
    },
    {
        name: "Jane Doe",
        email: "janedoe@yahoo.com",
        password: bcrypt.hashSync("1234", 10),
    },
];

export default users;
