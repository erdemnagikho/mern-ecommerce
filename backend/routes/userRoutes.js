import express from "express";
import {
    authUser,
    registerUser,
    getUserProfile,
    updateUserProfile,
} from "../controllers/userController.js";
const router = express.Router();
import { protect } from "../middleware/authMiddleware.js";

router.post("/login", authUser);
router
    .route("/profile")
    .get(protect, getUserProfile)
    .put(protect, updateUserProfile);
router.post("/", registerUser);

export default router;
